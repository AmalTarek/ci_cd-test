﻿using System;
using DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
  [TestClass]
  public class UnitTest1
  {
    [TestMethod]
    public void TestMethod1()
    {
      MainContext ctx = new MainContext(
#if DEV
        "main_db_dev"
#elif PROD
        "main_db_prod"
#elif LOCAL
        "main_db_local"
#endif

        );
      ctx.Versions.Add(new VersionTable
      {
        Time = DateTime.Now
      });
      ctx.SaveChanges();
    }

    [TestMethod]
    public void TestMethod2()
    {
      MainContext ctx = new MainContext(
#if DEV
        "main_db_dev"
#elif PROD
        "main_db_prod"
#elif LOCAL
        "main_db_local"
#endif

        );
      ctx.Versions.Add(new VersionTable
      {
        Time = DateTime.Now
      });
      ctx.SaveChanges();
    }
  }
}
