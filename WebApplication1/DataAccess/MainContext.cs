﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
  public class VersionTable
  {
    public virtual int Id { get; set; }
    public virtual DateTime Time { get; set; }
  }
  public class MainContext : DbContext
  {
    public MainContext(string con) : base(con)
    {

    }
    public MainContext() : base("main_db")
    {

    }
    public virtual DbSet<VersionTable> Versions { get; set; }
  }
}
