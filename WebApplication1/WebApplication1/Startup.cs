﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(WebApplication1.Startup))]

namespace WebApplication1
{
  public partial class Startup
  {
    public void Configuration(IAppBuilder app)
    {
      var exeNames = new[] { "msbuild.exe" };

      

      var pathDirs = Environment.GetEnvironmentVariable("PATH")?.Split(new[] { Path.PathSeparator }, StringSplitOptions.RemoveEmptyEntries);

      if (pathDirs?.Length > 0)
      {
        foreach (var exeName in exeNames)
        {
          var exePath = pathDirs.Select(dir => Path.Combine(dir, exeName)).FirstOrDefault(File.Exists);
          if (exePath != null)
          {
            System.Diagnostics.Debug.WriteLine("found it!");
          }
        }
      }

      ConfigureAuth(app);
    }
  }
}
